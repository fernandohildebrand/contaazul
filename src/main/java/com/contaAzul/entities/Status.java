package com.contaAzul.entities;

import java.util.HashMap;
import java.util.Map;

public enum Status {
	
	 PENDING("PENDING"), PAID("PAID"), CANCELED("CANCELED");
	
	private String status;
	
	private Status(String status) {
		
		this.status = status;
		
	}
	
	public String getStatus() {
		
		return this.status;
		
	}
	
    //****** Reverse Lookup Implementation************//
	 
    //Lookup table
    private static final Map<String, Status> lookup = new HashMap<>();
  
    //Populate the lookup table on loading time
    static
    {
        for(Status env : Status.values())
        {
            lookup.put(env.getStatus(), env);
        }
    }
  
    //This method can be used for reverse lookup purpose
    public static Status get(String status)
    {
        return lookup.get(status);
    }
    
    //This is a validation method to check incoming strings against Status values.
    public static boolean contains(String test) {

        for (Status status : Status.values()) {
            if (status.name().equals(test)) {
                return true;
            }
        }

        return false;
    }

}
