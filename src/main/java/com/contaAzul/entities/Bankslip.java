package com.contaAzul.entities;

import java.time.LocalDate;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;



@Entity
@Table(name = "bankslip")
public class Bankslip {
	
	
	
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator( name = "UUID",
    		strategy = "org.hibernate.id.UUIDGenerator")
    private UUID  id;

    @Column(name = "customer")
    @NotNull
    @NotBlank(message = "customer can't empty!")
    private String customer;
    
    @Column(name = "status")
    @NotNull
    @NotBlank(message = "status can't empty!")
    private String status;
    
    @Column(name="totalInCents")
    @NotNull
    @JsonProperty("total_in_cents")
    private int totalInCents;
    
    @Column(name="dueDate", columnDefinition = "DATE")
    @NotNull
    @JsonProperty("due_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dueDate;
    
    private Bankslip() {} //JPA only.
    
    public Bankslip(final String customer, final String status, final int totalInCents, 
    		final LocalDate dueDate) 
    {
    
    	this.customer = customer;
    	this.status = status;
    	this.totalInCents = totalInCents;
    	this.dueDate = dueDate;
    	
    }

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String name) {
		this.customer = name;
	}

	public String getStatus() {
		
		return this.status;
	}

	public void setStatus(String status) {
		if (Status.contains(status)) {
			this.status = status;
		}
		
	}

	public int getTotalInCents() {
		return totalInCents;
	}

	public void setTotalInCents(int totalInCents) {
		this.totalInCents = totalInCents;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public LocalDate getDueDate() {
		return this.dueDate;	
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public void setDueDate(LocalDate dueDate) {		
		this.dueDate = dueDate;
	}

}
