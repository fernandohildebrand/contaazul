package com.contaAzul.entities.DTO;


import java.time.LocalDate;
import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonProperty;

public interface BankslipGetAllProjection {
	
	
	public UUID getId();
	
	public String getCustomer();
	
	@JsonProperty("total_in_cents")
	public int getTotalInCents();
	
	@JsonProperty("due_date")
	public LocalDate getDueDate();

}
