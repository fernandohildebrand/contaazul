package com.contaAzul.entities.DTO;


import java.time.LocalDate;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonProperty;


/*Bankslip Data Transfer Object. In order to receive RESTfull Jason calls specified by Conta Azul, 
 * and process any required information to map to a Bankslip object. So input is agreeable with 
 * JPA specifications. 
 * Also performs data validations according to Conta Azul specifications.
 */


public class BankslipPostDTO {
	

	@NotNull
	@NotBlank(message = "customer can't empty!")
    private String customer;
    
	@NotNull
	@NotBlank(message = "status can't empty!")
    private String status;
	
    @JsonProperty("total_in_cents")
    @NotNull(message = "total_in_cents can't empty!")
    private int totalInCents;
    
    @JsonProperty("due_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "due_date can't empty!")
    private LocalDate dueDate;
    
    public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTotalInCents() {
		return totalInCents;
	}

	public void setTotalInCents(int totalInCents) {
		this.totalInCents = totalInCents;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

      
}
