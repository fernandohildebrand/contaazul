package com.contaAzul.entities.DTO;

import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class BankslipPutDTO {

	@NotNull
	private UUID id;
	
	@NotNull
	@NotBlank(message = "status can't empty!")
    private String status;
	
	public BankslipPutDTO(UUID id, String status) {
		this.id = id;
		this.status = status;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
