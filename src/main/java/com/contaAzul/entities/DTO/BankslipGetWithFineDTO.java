package com.contaAzul.entities.DTO;




import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;

import com.contaAzul.utils.UtilitiesMethods;
import com.fasterxml.jackson.annotation.JsonProperty;

/*Bankslip Data Transfer Object. In order to receive RESTfull Jason calls specified by Conta Azul, 
 * and process any required information to map to a Bankslip object. So input is agreeable with 
 * JPA specifications. 
 * Also performs data validations according to Conta Azul specifications.
 */


public class BankslipGetWithFineDTO {

	@Pattern(regexp="^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$")
	private UUID id;
	
    @NotBlank(message = "customer can't empty!")
	private String customer;

	@NotBlank(message = "total_in_cents can't empty!")
    @JsonProperty("total_in_cents")
	private int totalInCents;

	@JsonProperty("due_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dueDate;
	
	@NotBlank(message = "status can't empty!")
	private String status;
	
	@NotBlank(message = "status can't empty!")
	private int fine;
	
	public BankslipGetWithFineDTO(UUID id, String customer, int totalInCents, LocalDate dueDate, String status) {
		
		this.id= id;
		this.customer = customer;
		this.totalInCents = totalInCents;
		this.dueDate = dueDate;
		this.status = status;
		this.fine= 0;
		this.calculateFine();
		
	}

	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public int getTotalInCents() {
		return totalInCents;
	}

	public void setTotalInCents(int totalInCents) {
		this.totalInCents = totalInCents;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public LocalDate getDueDate() {
		return dueDate;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getFine() {
		return fine;
	}

	public void setFine(int fine) {
		this.fine = fine;
	}

	private void calculateFine() {
		
		
		int daysDifference = UtilitiesMethods.daysBetween(LocalDate.now(), 
				this.getDueDate()); 
		
		if(daysDifference > 0) {
			if(daysDifference < 10 ) {
				this.setFine(UtilitiesMethods.fine05(this.getTotalInCents()));
			}else if(daysDifference > 9) {
				this.setFine(UtilitiesMethods.fine1(this.getTotalInCents()));
			}
			
		}
		
	}
	
	
	
}
