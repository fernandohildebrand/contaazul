package com.contaAzul.restEndpoints;

import java.net.URI;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.contaAzul.entities.Bankslip;
import com.contaAzul.entities.DTO.BankslipGetWithFineDTO;
import com.contaAzul.entities.DTO.BankslipGetAllProjection;
import com.contaAzul.entities.DTO.BankslipPostDTO;
import com.contaAzul.entities.DTO.BankslipPutDTO;
import com.contaAzul.repositories.*;
import com.contaAzul.restEndpoints.exceptions.BankslipGetUUIDException;
import com.contaAzul.restEndpoints.exceptions.BankslipNotFoundException;
import com.contaAzul.utils.MessageApi;
import com.contaAzul.utils.UtilitiesMethods;


// This is the class responsable for handling all of Bankslip RESTfull services.

@RestController
@RequestMapping("/rest/bankslips")
public class BankslipRestController {
	
	@Autowired
	BankslipRepository bankslipRepository;
	
	@PostMapping
	ResponseEntity<?> add(@Valid @RequestBody BankslipPostDTO input){	

		Bankslip result = bankslipRepository.save(new Bankslip(input.getCustomer(), 
				input.getStatus(), input.getTotalInCents(), input.getDueDate() ) );
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(result.getId()).toUri();
		
		System.out.println(location);
		
		return new ResponseEntity( new MessageApi(HttpStatus.CREATED,"Bankslip created."),HttpStatus.CREATED );

	}
	
	@GetMapping
	Collection<BankslipGetAllProjection> bankSlipList(){
		
		return this.bankslipRepository.findAllProjectedBy();
		
	}
	
	@GetMapping("/{id}")
	BankslipGetWithFineDTO bankSlipDetail(@PathVariable String id){	
		
		if(! UtilitiesMethods.checkUUID(id)) {
			throw new BankslipGetUUIDException();
		}
				
		BankslipGetWithFineDTO bankslipDTO = this.bankslipRepository.
				FindByIdAsDTO(UUID.fromString(id));
		
		if (bankslipDTO == null) {
			throw new BankslipNotFoundException();
		}
		
		return bankslipDTO;
		
	}
	
	@PutMapping("/{id}/pay")
	ResponseEntity<?> putSave(@Valid @RequestBody BankslipPutDTO input ){
		
		
		Bankslip bankslip = this.bankslipRepository.findById(input.getId())
				.orElseThrow( () -> new BankslipNotFoundException() );
		
		bankslip.setStatus("PAID");
		Bankslip result = bankslipRepository.save(bankslip);
		
		return new ResponseEntity( new MessageApi(HttpStatus.OK,"Bankslip paid."),HttpStatus.OK );
		
	}
	
	@DeleteMapping("/{id}/cancel")
	ResponseEntity<?> deleteBankslip(@Valid @RequestBody BankslipPutDTO input ){
		
		
		Bankslip bankslip = this.bankslipRepository.findById(input.getId())
				.orElseThrow( () -> new BankslipNotFoundException() );
		
		bankslip.setStatus(input.getStatus());
		Bankslip result = bankslipRepository.save(bankslip);
		
		return new ResponseEntity( new MessageApi(HttpStatus.OK,"Bankslip canceled."),HttpStatus.OK );
	}
		
	

}
