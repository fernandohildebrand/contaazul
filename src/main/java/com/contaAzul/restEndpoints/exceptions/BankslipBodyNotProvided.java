package com.contaAzul.restEndpoints.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BankslipBodyNotProvided extends RuntimeException {
	
	public BankslipBodyNotProvided() {
		super("Bankslip not provided in the request body");
	}

}
