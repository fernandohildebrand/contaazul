package com.contaAzul.restEndpoints.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BankslipGetUUIDException extends RuntimeException {

	public BankslipGetUUIDException() {
		super("Invalid id provided - it must be a valid UUID");
	}

}
