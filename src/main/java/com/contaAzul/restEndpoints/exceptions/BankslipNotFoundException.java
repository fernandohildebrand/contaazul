package com.contaAzul.restEndpoints.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BankslipNotFoundException  extends RuntimeException{
	
	public BankslipNotFoundException(){
		super("Bankslip not found with the specified id");
	}

}
