package com.contaAzul.restEndpoints;

import org.springframework.web.bind.annotation.ResponseStatus;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.contaAzul.restEndpoints.exceptions.BankslipBodyNotProvided;
import com.contaAzul.utils.MessageApi;



@RestControllerAdvice
public class BankslipCustomExceptionHandler extends ResponseEntityExceptionHandler{
	

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
		
		//System.out.println(ex);
            		
	    return new ResponseEntity<>(
	    		new MessageApi(HttpStatus.BAD_REQUEST,
	    				"Not found.",
	    				"Bankslip not provided in the request body",
	    				"/rest/bankslip"),
	    		new HttpHeaders(),
	    		HttpStatus.BAD_REQUEST); 
	} 
	

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, 
			HttpHeaders headers, 
			HttpStatus status, 
			WebRequest request) {
		
		return new ResponseEntity<>(
	    		new MessageApi(HttpStatus.UNPROCESSABLE_ENTITY,
	    				"Not found.",
	    				"Invalid bankslip provided.The possible reasons are:" +
	    				" A field of the provided bankslip was null or with invalid values)",
	    				"/rest/bankslip"), HttpStatus.UNPROCESSABLE_ENTITY);
	}

}
