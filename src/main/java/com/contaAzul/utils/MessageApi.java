package com.contaAzul.utils;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

public class MessageApi {
	
	private LocalDateTime timestamp;
	private HttpStatus status;
	private String error;
	private String message;
	private String path;
	
	/*Use jthis constructor to create a successful Responseentity return. 
	 * IMPORTANT! Only use this in case of 200+ Http Status.
	 */
	public MessageApi(HttpStatus status, String message) {
		
		this.timestamp = LocalDateTime.now();
		this.error= "";
		this.status = status;
		this.message = message;
	}
	
	/*Use this constructor for all others error messages
	 */
	public MessageApi(HttpStatus status, String error, String message, String path) {
		
		this.timestamp = LocalDateTime.now();
		this.error = error;
		this.status = status;
		this.message = message;
		this.path = path;
	}
	
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
}
