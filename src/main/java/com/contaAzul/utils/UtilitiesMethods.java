package com.contaAzul.utils;

import java.time.LocalDate;
import java.time.Period;
import java.util.regex.Matcher;
import java.util.regex.Pattern; 


public final class UtilitiesMethods {
	
	
	/*Get now for java.sql.Date
	public static Date getNow() {
		
		java.util.Date nowUtil= new java.util.Date();
		Date now = new java.sql.Date(nowUtil.getTime());
		return now;
	} */
	
	//Static method that compares two  dates and returns the difference.
	public static int daysBetween(LocalDate now, LocalDate before) {
		
		//LocalDate nowLocal = now.toLocalDate();
		//LocalDate beforeLocal = before.toLocalDate();
		
		Period gap = Period.ofDays(0);
		
		gap = Period.between(now, before);
		
		return Math.abs(gap.getDays());		
        
    }
	
	//Fine calculator at 0.5%
	public static int fine05(int value) {
		
		return (int) Math.round( value +  ((value * 0.5)/ 100)  );
	}
	
	//Fine calculator at 1%
	public static int fine1(int value) {
			
		return (int) Math.round( value +  (value / 100)  );
	}
	
	public static boolean checkUUID(String uuid) {
		
		final Pattern UUIDPATERN = 
				Pattern.compile("^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$");
		
		Matcher matcher = UUIDPATERN.matcher(uuid);

	    if (!matcher.matches()) { 
	    	System.out.println("FALSO========================");
	    	return false; 	
	    } 
	    System.out.println("VERDADE========================");
		return true;
	}

}
