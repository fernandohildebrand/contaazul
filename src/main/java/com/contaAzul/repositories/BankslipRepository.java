package com.contaAzul.repositories;

import com.contaAzul.entities.Bankslip;
import com.contaAzul.entities.DTO.*;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 * All transactions in this class are marked as read only.  If a transaction needs to modify the datasource
 * mark the method modifying the datasource with @Modifying and @Transactional annotation.
 *
 * @see <a href=https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#transactional-query-methods </a>
 */


public interface BankslipRepository extends JpaRepository<Bankslip, UUID> {

	
	//Public method to save and commit a Bankslip Object into the choosen database.
	public Bankslip save(Bankslip bankslip);
	
	//Public method to retrieve all Bankslips thought projection,
	Collection<BankslipGetAllProjection> findAllProjectedBy();

	
	//Public method to retrieve a BankslipDTO with 'fine' field.	
	@Query("SELECT new com.contaAzul.entities.DTO.BankslipGetWithFineDTO(b.id, b.customer, b.totalInCents, b.dueDate, b.status) FROM Bankslip b  WHERE b.id = :id")
    BankslipGetWithFineDTO FindByIdAsDTO(@Param("id") UUID id);
		

}