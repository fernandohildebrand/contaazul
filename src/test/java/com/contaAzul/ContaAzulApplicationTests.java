package com.contaAzul;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import java.util.UUID;
import com.contaAzul.entities.Bankslip;
import com.contaAzul.entities.DTO.BankslipPutDTO;
import com.contaAzul.repositories.BankslipRepository;
import com.contaAzul.restEndpoints.BankslipRestController;




@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class ContaAzulApplicationTests {
	
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private List<Bankslip> bankslipList;
    
    private Bankslip bankslip;

    private List<Bankslip> bookmarkList = new ArrayList<>();
    
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    private BankslipRepository bankslipRepository;
    
    @Autowired
    private BankslipRestController bankslipController;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
            .findAny()
            .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }
    
    @Before
    public void setup() throws Exception {
    	
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        this.bankslipRepository.deleteAllInBatch();
        
        this.bankslipRepository.save(new Bankslip("Valiant Customer",
        		"PENDING",
        		100000,
        		LocalDate.parse("2018-01-01",formatter)) );
        
        this.bankslipRepository.save(new Bankslip("Coward Customer",
        		"PENDING",
        		100000,
        		LocalDate.now() ) );
        
        this.bankslipList = this.bankslipRepository.findAll();
        
    }
    
	@Test
	public void postSaveSuccess()  throws Exception{
		
		 mockMvc.perform(post("/rest/bankslips")
	                .content(this.json(new Bankslip("Valid customer", "PENDING", 100000,LocalDate.now() ) ) )
	                .contentType(contentType))
	                .andExpect(status().isCreated());
	}
	
	@Test
	public void postIllBody() throws Exception {
		
		mockMvc.perform(post("/rest/bankslips")
                .content(this.json(null))
                .contentType(contentType))
                .andExpect(status().isBadRequest());
		
	}
	
	@Test
	public void postMalFormed()  throws Exception{
		mockMvc.perform(post("/rest/bankslips")
                .content(this.json(new Bankslip("", "", 100000 ,LocalDate.now())))
                .contentType(contentType))
                .andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void getAll()  throws Exception{
		
		mockMvc.perform(get("/rest/bankslips"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
		
	}
	
	@Test
	public void getOne()  throws Exception{
		
		System.out.println("ID!!!!!!!!!!!!!!!!   " + this.bankslipList.get(0).getId().toString());
		
		mockMvc.perform(get("/rest/bankslips/"
                + this.bankslipList.get(0).getId().toString())
				)
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.customer", is("Valiant Customer")));
		
	}
	
	@Test
	public void putTest()  throws Exception{
		
		UUID id = this.bankslipList.get(0).getId();
		String idString = id.toString();
		
		mockMvc.perform(put("/rest/bankslips/" + idString + "/pay" )
                .content(this.json(new BankslipPutDTO(id, "PAID")))
                .contentType(contentType))
                .andExpect(status().is2xxSuccessful());
		
	}
	
	@Test
	public void deleteTest()  throws Exception{
		
		UUID id = this.bankslipList.get(0).getId();
		String idString = id.toString();
		
		mockMvc.perform(delete("/rest/bankslips/" + idString + "/cancel" )
                .content(this.json(new BankslipPutDTO(id, "CANCELED")))
                .contentType(contentType))
                .andExpect(status().is2xxSuccessful());
		
	}
	
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
	
}
