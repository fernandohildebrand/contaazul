README
What is this repository for?

    Quick summary - This repository serves as an example of technological choices that were made to solve the following problems: Fast Prototyping of 
    a persistence layer with RESTful boundaries.


How do I get set up?

    Summary of set up - This project uses spring boot which drops a war to be consumed by some container. In running this example there is 
    a container to execute the war.
        I used Eclipse(Latest) with Spring Boot IDE extension, JDK 1.8.
        

    How to run tests - 
    The easy way is to use Eclipse with Spring Boot IDE instaled. Just clone the git repository from 
    "https://bitbucket.org/fernandohildebrand/contaazul/src/master/" and import in Eclipse as a Existing Maven Project. 
    Run the application with: RUN->RUN-AS>Spring Boot. And to run Junit tests, simply select src/test/java in Package explorer 
    with mouse right click and choose RUN AS-> Junit Test. 
    	To make calls to the service with tool of choice. The RESTClient plugin in Firefox to test PUT, GET, POST and DELETE, or 
        use curl as shown bellow.
    	OBS: To use a RESTClient just copy the body from curl examples.
    	
    	Ex:
    	1- curl -X POST -i http://localhost:8080/rest/bankslips/ --data '{
    		"due_date": "2018-01-01",
    		"total_in_cents": "100000",
    		"customer": "Trillian Company",
    		"status": "PENDING"
    		}'
		2- curl -X GET -i http://localhost:8080/rest/bankslips/



    The application can be run without IDE using: mvn spring-boot:run
    If desired, can be run in debug mode using: mvn spring-boot:run -X

IMPORTANT NOTE!!!!!!!!!!
AS THE TEST ONLY SPECIFIED ONE FIELD RELEVANT TO THE CUSTOMER IT WWAS A BEST STRATEGY TO USE ONLY ONE TABLE, THUS AVOIDING UNNECESSARY JOINS. ALSO MAKING EASIER TO EVENTUALLY MOVE FROM A RELATIONAL DATABASE TO A LIST ONE, SUCH AS MONGODB.
